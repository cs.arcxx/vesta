<?php get_header(); ?>
    <div class="container">
    <div class="notf">
        <hr/>
        <h1 class="text-center"><?php _e('Error 404'); ?></h1>
        <p class="text-center"><?php _e('Page not found!'); ?></p>
        <hr/>
    </div>
<?php get_footer(); ?>