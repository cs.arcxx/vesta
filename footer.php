<footer id="footer">
    <div class="container">
        <div id="cloud-tags">
            <?php $args = array(
                'smallest' => 8,
                'largest' => 12,
                'unit' => 'pt',
                'number' => 99,
                'format' => 'flat',
                'orderby' => 'name',
                'link' => 'view',
                'taxonomy' => 'post_tag',
            ); ?>
            <?php wp_tag_cloud($args); ?>
        </div>

        <div class="divider"></div>
        <p id="copyright">
            <a href="https://gitlab.com/cs.arcxx/vesta">قالب وستا </a> از <a href="http://arcxx.ir/">فرهاد</a>
            به الهام از <a href="https://github.com/TheYahya/thewhite">قالب سفید</a>
        </p>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
